CREATE TABLE simplejdbc.MyJavaNote
(
    ID    INT(16) AUTO_INCREMENT NOT NULL,
    date  DATETIME NULL,
    title VARCHAR(265) NULL,
    note  TEXT NULL,
    PRIMARY KEY (ID)
);

INSERT INTO simplejdbc.MyJavaNote (date, title, note) VALUES ('2021-06-27','My first note', 'OMG, does it work?????');
INSERT INTO simplejdbc.MyJavaNote (date, title, note) VALUES ('2019-06-28','The second note', 'Cool, looks like it works!');
INSERT INTO simplejdbc.MyJavaNote (date, title, note) VALUES ('2019-05-29','Is this the 3rd one?', 'I thought I remember it without writing it down....');