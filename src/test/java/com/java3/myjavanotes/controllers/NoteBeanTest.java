package com.java3.myjavanotes.controllers;

import com.java3.myjavanotes.models.JDBCNote;
import com.java3.myjavanotes.models.Note;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Defines how a noteBean gets tested
 * Doesn't contain Tests for DB
 *
 * @author Oliver Huber
 * @see com.java3.myjavanotes.models.Note
 */
class NoteBeanTest {
    private static NoteBean noteBean;
    private static Note note;
    private static String pattern = "dd.MM.yyyy";
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
    private static DateFormat df;
    JDBCNote jdbcNote;

    @BeforeEach
    void setUp() {
        note = new Note();
        df = new SimpleDateFormat(pattern);
        formatter = DateTimeFormatter.ofPattern(pattern);
        noteBean = new NoteBean();
        jdbcNote = new JDBCNote();
    }

    @AfterEach
    void tearDown() {
        note = null;
        jdbcNote = null;
        df = null;
        noteBean = null;
        formatter = null;

    }
    @Test
    void create() {
        assertEquals(noteBean.create(), "detailedNote.xhtml?faces-redirect=true");;
        assertTrue(noteBean.getDateText().equals(""));
        assertTrue(noteBean.getTitle().equals(""));
        assertTrue(noteBean.getNotiz().equals(""));
        assertEquals(noteBean.getSiteTitle(), "Notiz erfassen");
        assertEquals(noteBean.getBackButton(), "Create");
        assertFalse(noteBean.isOnlyView());
    }

    @Test
    void getCurrentDate() {
        assertFalse(noteBean.getCurrentDate().isEmpty());
    }


}