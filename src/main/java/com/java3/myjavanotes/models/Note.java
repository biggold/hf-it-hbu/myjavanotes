package com.java3.myjavanotes.models;

import java.time.LocalDate;

/**
 * Defines the Note
 *
 * @author Cyrill Näf
 */
public class Note {
    private Integer id;
    private LocalDate date;
    private String title;
    private String note;
    private Boolean isNull = false;

    /**
     * Note without ID
     *
     * @param date  Note Date
     * @param title Note Title
     * @param note  Note Content, the actual note
     */
    public Note(LocalDate date, String title, String note) {
        this.id = null;
        this.date = date;
        this.title = title;
        this.note = note;
    }

    /**
     * Note with ID
     *
     * @param id    Note identifier
     * @param date  Note Date
     * @param title Note Title
     * @param note  Note Content, the actual note
     */
    public Note(Integer id, LocalDate date, String title, String note) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.note = note;
    }

    /**
     * Empty Note
     */
    public Note() {
        this.isNull = true;
    }

    public Boolean isNull() {
        return isNull;
    }

    public Integer getId() {
        return id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
