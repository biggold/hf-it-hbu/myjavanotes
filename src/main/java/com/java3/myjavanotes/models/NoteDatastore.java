package com.java3.myjavanotes.models;

import java.util.List;

/**
 * Interface describing how the Note Datastore
 *
 * @author Cyrill Näf
 */
public interface NoteDatastore {
    /**
     * Get's a specific note from the datastore
     *
     * @param id ID of the note
     * @return The note as Note object
     */
    public Note getNote(int id);

    /**
     * Creates a new note in the datastore
     *
     * @param note The Note object
     */
    public void createNote(Note note);

    /**
     * Deletes a note in the datastore
     *
     * @param note The Note object (ID field is mandatory!)
     */
    public void deleteNote(Note note);

    /**
     * Deletes a note in the datastore
     *
     * @param id The id of a note
     */
    public void deleteNote(int id);

    /**
     * Returns a List of all notes objects in the datastore
     *
     * @return List of Notes object
     */
    public List<Note> getNotes();

    /**
     * Updates a note object
     *
     * @param note The Note object (ID field is mandatory!)
     * @return Returns the updated Note object
     */
    public Note updateNote(Note note);
}
